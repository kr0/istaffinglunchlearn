let Highlight = require('react-highlighter');

import * as React from "react";

export interface AppProps {
    name:string,
    names:string[]
}

export interface AppState {
    value:string
}


export default class App extends React.Component<AppProps, AppState> {

    constructor() {
        super();
        this.state = {
            value: ''
        }
    }

    __handleValueChange(e) {
        this.setState({value: e.target.value});
    }

    render() {

        const peopleHighlightingThing =
            (<div>
                <input value={this.state.value} onChange={(e)=> this.__handleValueChange(e)}/>
                <div className="people-names">
                    <Highlight
                        search={this.state.value}
                        matchClass="highlighted">{this.props.names}</Highlight>
                </div>
            </div>);
        
        

        return (
            <div>
                <h1>Hello, {this.props.name}!</h1>
                <hr/>
                {peopleHighlightingThing}
            </div>
        );
    }
}





