require( "!style!css!./some.css");
import * as React from "react";
import * as ReactDom from "react-dom"
import App from "./App.tsx";

const peopleNames:string[] = ["Kelvin", "Karlina", "DeeDee", "aKilla"];


ReactDom.render(<App name="Typescript" names={peopleNames.join(" ")}/>, document.getElementById('root'));
